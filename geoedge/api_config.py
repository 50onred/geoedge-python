from collections import namedtuple
from functools import partial


APIEndpoint = namedtuple('APIEndpoint', 'endpoint method paginated')
DefaultAPIEndpoint = partial(APIEndpoint, paginated=False)
GETEndpoint = partial(DefaultAPIEndpoint, method='get')
GETPaginatedEndpoint = partial(GETEndpoint, paginated=True)
POSTEndpoint = partial(DefaultAPIEndpoint, method='post')
PUTEndpoint = partial(DefaultAPIEndpoint, method='put')
DELETEEndpoint = partial(DefaultAPIEndpoint, method='delete')


api_endpoints = {
        'projects': {
            'list': GETPaginatedEndpoint('projects'),
            'get': GETEndpoint('projects/{project_id}'),
            'add': POSTEndpoint('projects'),
            'add_multiple': POSTEndpoint('projects/bulk'),
            'edit': PUTEndpoint('projects/{project_id}'),
            'delete': DELETEEndpoint('projects/{project_ids}'),
            'search': GETEndpoint('projects'),
            'scan': POSTEndpoint('projects/{project_ids}/scan'),
            'scan_status': GETEndpoint('projects/{scan_id}/scan-status')
            },
        'alerts': {
            'list': GETPaginatedEndpoint('alerts'),
            'list_categories': GETEndpoint('list_categories'),
            'add': POSTEndpoint('alerts'),
            'edit': PUTEndpoint('alerts/{alert_id}'),
            'delete': DELETEEndpoint('alerts/{alert_ids}'),
            'projects_bind': POSTEndpoint('alerts/{alert_id}/projects-bind'),
            'trigger_types': GETEndpoint('alerts/trigger-types'),
            'get': GETEndpoint('alerts/{alert_id}')
            },
        'alerts_history': {
            'get': GETEndpoint('alertshistory/{alerts_history_id}'),
            'search': GETPaginatedEndpoint('alerts/history'),
            'whitelist': POSTEndpoint('alertshistory/{history_id}/whitelist'),
            },
        'ads': {
            'get': GETEndpoint('ads/{ad_id}'),
            'search': GETEndpoint('ads'),
            },
        'landing_pages': {
            'search': GETEndpoint('lps'),
            },
        'locations': {
            'list': GETEndpoint('locations'),
            },
        'platforms': {
            'list': GETEndpoint('platforms'),
            },
        'emulation_sets': {
            'list': GETEndpoint('emulation-sets'),
            },
        'emulators': {
            'list': GETEndpoint('emulators'),
            },
        'networks': {
            'list': GETEndpoint('networks'),
            },
        'usage': {
            'list': GETEndpoint('usage'),
            }
        }
