from setuptools import find_packages, setup


setup(
    name='geoedge-python',
    version='0.3',
    url='https://bitbucket.org/50onred/geoedge-python',
    license='BSD',
    author='Craig Slusher',
    author_email='craig@red-spark.com',
    description='A python API for GeoEdge',
    long_description=__doc__,
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'requests>=2.18.1',
        'six>=1.8.0'
    ],
    #tests_require=[
    #    'nose'
    #],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python 2',
        'Programming Language :: Python 3',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
