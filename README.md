# Usage

```python
from geoedge.api import GeoEdgeAPI

api = GeoEdgeAPI('my api key here')

# list all projects
print api.projects.list()

# get a project
print api.projects.get(project_id='abcdefg')
```
